orbit
=====

Types and functions for dealing with Kepler orbits.

The main data type is Orbit, which describes the path of a body in orbit.

Implementation
--------------

This package makes use of the
[`uom-plugin`](https://hackage.haskell.org/package/uom-plugin) package to make
sure that the implementation is correct regarding units of measure.


Contributing
------------

Contributions and bug reports are welcome!

Please feel free to contact me on GitHub or as "jophish" on freenode.

-Joe

